# v1.4.2 (2017-10-17)
* Added sudo-prompt to allow password prompt while load spinner is displayed

# v1.4.1 (2017-10-17)
* Fixed bug where the increment command would fire when not wanted

# v1.4.0 (2017-10-17)
* Added `update, -u` commands to allow updating to the latest version of `farp`

# v1.3.0 (2017-10-17)
* Added `version, -v` commands to view the current version of `farp`

# v1.2.0 (2017-10-17)
* Added `update` command to allow updating to the latest version of `farp`

# v1.1.0 (2017-10-15)
* Initial Release
