const chalk = require('chalk');
const fs = require('fs');
const sprintf = require('sprintf-js').sprintf;
const path = require('path');
const say = console.log;
var package = require('../package.json');

module.exports = {
     getArg: function(arg) {
         return process.argv[arg + 1];
     },
     help: function() {
         say(sprintf(" "));
         say(sprintf(chalk.magenta.bold("--------  FARP v" + package.version + " (Find and Replace) Help  --------")));
         say(sprintf(" "));
         say(chalk.blue.bold(sprintf("Usage")));
         say(chalk.cyan.bold(sprintf("  farp [<command>]   ")) + 'Where <command> is optional, or any of the following:');
         say(sprintf(" "));
         say(chalk.blue.bold(sprintf("Commands")));
         say(chalk.cyan.bold(sprintf("  -i, -iz            ")) + 'Adds a digit preceded by a hyphen to the end of every file name, incrementing from 1+ in ascending order without a leading zero.');
         say(chalk.cyan.bold(sprintf("                     ")) + 'When "z" is used the incremented numbers will contain a leading zero.');
         say(chalk.red.bold(sprintf("                       Note:")) + ' When this command is used, the find/replace functionality is skipped all together.');
         say(sprintf(" "));
         say(chalk.cyan.bold(sprintf("  -r, regex          ")) + 'Sets the "Find" value to be processed as a regular expression.');
         say(sprintf(" "));
         say(chalk.cyan.bold(sprintf("  -v, version        ")) + 'Displays the current version of FARP.');
         say(sprintf(" "));
         say(chalk.cyan.bold(sprintf("  -u, update         ")) + 'Installs the latest version of FARP.');
         say(sprintf(" "));
         say(chalk.cyan.bold(sprintf("  -h, help           ")) + 'Displays this help menu.');
         say(sprintf(" "));
         say(chalk.blue.bold(sprintf("RegEx Flags (optional)")));
         say(sprintf("  Pass the flag after the RegEx separated by a space (i.e. Find: \\d+ -g). Defaults to a single match if left blank."));
         say(sprintf(" "));
         say(chalk.cyan.bold(sprintf("  -g                 ")) + 'Global match; find all matches rather than stopping after the first match.');
         say(chalk.cyan.bold(sprintf("  -i                 ")) + 'Ignore case.');
         say(sprintf(" "));
     },
     /**
      * Either update file names or display a preview of what files will be updated
      * param   Object     files      Actual files
      * param   Bool       dryRun     Whether or not to actually update the files
      * return  Void
      */
     maybeUpdateFiles: function(settings, dryRun) {
         if(!dryRun) {
             dryRun = false;
         }

         let counter = 0;

         settings.files.forEach(function(file, index) {
             theFile = file;
             let ext = path.extname(theFile),
                 basename = path.basename(theFile, ext),
                 newFileName;

             // Support regex
             if(settings.args == '-r' || settings.args == 'regex') {
                 let flag = settings.needle.split(' -')[1] ? settings.needle.split(' -')[1] : '';
                 let regex = new RegExp(settings.needle.split(' -')[0].trim(), flag);
                 updatedFileName = basename.replace(regex, settings.replacement)
             } else {
                 updatedFileName = basename.replace(settings.needle, settings.replacement);
             }

             // Keep track of how many file names were updated and log the update
             // -------------------------------------------------------------------------------
             if(basename != updatedFileName) {
                 if(dryRun) {
                     counter += 1;
                 }

                 // Update the file name and tack on the extension
                 newFileNameToTest = updatedFileName + ext;

                 // Check for duplicates
                 if(fs.existsSync(newFileNameToTest)) {
                     updatedFileName += '_copy';
                     newFileNameToTest = updatedFileName + ext;

                     while(fs.existsSync(newFileNameToTest)) {
                         updatedFileName += ' copy';
                         newFileNameToTest = updatedFileName + ext;
                     }

                 }

                 // Replace the file names and display what change will be made
                 newFileName = theFile.replace(theFile, (updatedFileName + ext));

                 if(dryRun) {
                     say(chalk.red(theFile) + '->' + chalk.green(newFileName));
                 } else {
                     // Save the file name change
                     fs.renameSync(theFile, newFileName);
                 }

             }
         });

         return counter;
     }
 }
