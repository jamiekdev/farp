#!/usr/bin/env node
const readline = require('readline');
const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const helpers = require('./functions/helpers');
const say = console.log;
const child_process = require('child_process');
const clear = require('clear');
var Spinner = require('cli-spinner').Spinner;
var spinner = new Spinner('%s Updating');
spinner.setSpinnerString('⠋⠙⠹⠸⠼⠴⠦⠧⠇⠏');
var package = require('./package.json');

// Create the readline instance
// -------------------------------------------------------------------------------
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let settings = {},
    updating = false;

// Check if any args were passed
// -------------------------------------------------------------------------------
let args = helpers.getArg(1) ? helpers.getArg(1) : '';
settings.args = args;

if(helpers.getArg(1) == 'help' || helpers.getArg(1) == '-h') {
    helpers.help();
    process.exit();
}

// Get the current version of FARP - eventually put into helpers
// Also add to help command
// -------------------------------------------------------------------------------
if(helpers.getArg(1) != undefined && (helpers.getArg(1).toLowerCase() == 'version' || helpers.getArg(1).toLowerCase() == '-v')) {
    say('v' + package.version);
    say(' ');
    rl.close();
    process.exit();
}

var sudo = require('sudo-prompt');
var options = {
  name: 'FARP',
  // icns: '/Applications/Electron.app/Contents/Resources/Electron.icns', // (optional)
};

// Update Farp to the latest version - eventually put into helpers
// Also add to help command
// -------------------------------------------------------------------------------
if(helpers.getArg(1) != undefined && (helpers.getArg(1).toLowerCase() == 'update' || helpers.getArg(1).toLowerCase() == '-u')) {
    spinner.start();
    updating = true;

    sudo.exec('npm install --ignore-scripts -g https://jamiekdesign@bitbucket.org/jamiekdesign/farp.git', options,
        function(error, stdout, stderr) {
            spinner.stop(true);
            if (error) throw error;
            // say(stderr);
            say(chalk.cyan('FARP sucsessfully updated to the most recent version.'));
            say(' ');
            say(stdout);
            rl.close();
            process.exit();
        }
    );
}

if(!updating) {
    if(args == '-i' || args == '-iz') {
        let counter = 0;

        // Read the current directory and get the files
        // -------------------------------------------------------------------------------
        fs.readdir('./', function(err, files) {
            if( err ) {
                console.error("Could not list the directory.", err);
                process.exit(1);
            }

            files.forEach(function(file, index) {
                counter += 1;
                let ext = path.extname(file),
                    basename = path.basename(file, ext),
                    newFileName;

                // Check is we're using a zero index
                if(args == '-iz') {
                    basename += counter < 10 ? '-' + '0' + counter : '-' + counter;
                } else {
                    basename += '-' + counter
                }

                newFileName = file.replace(file, (basename + ext));

                // Save the file name change
                fs.renameSync(file, newFileName);
                say(chalk.red(file) + '->' + chalk.green(newFileName));
            });

            say(chalk.green('\n\rSuccess!'));
        });

        // Close the readline input stream
        rl.close();
    } else {
        // Get user input for what to replace
        // -------------------------------------------------------------------------------
        rl.question(chalk.cyan('Find: '), (needle) => {
            settings.needle = needle;

            // Get user input for replacement
            // -------------------------------------------------------------------------------
            rl.question(chalk.cyan('Replace with: '), (replacement) => {
                let message = '';

                settings.replacement = replacement;

                // Read the current directory and get the files
                // -------------------------------------------------------------------------------
                fs.readdir('./', function(err, files) {
                    if( err ) {
                        console.error("Could not list the directory.", err);
                        process.exit(1);
                    }

                    settings.files = files;

                    var newFileName,
                        theFile;

                    // Loop through the files and generate a preview of the changes that will be made
                    // -------------------------------------------------------------------------------
                    let counter = helpers.maybeUpdateFiles(settings, true);

                    // If no matches were found then close it down
                    // -------------------------------------------------------------------------------
                    if(counter == 0) {
                        say('No matches were found. ' + chalk.red('Exiting') + '.');
                        say(' ');
                        rl.close();
                        process.exit();
                    } else {
                        function getVarification(invalid) {
                            let text = '';
                            if(invalid == false) {
                                text = 'The above changes will be made. Are you sure you want to proceed? ' + chalk.cyan.bold('(y/n) ');
                            } else {
                                text = 'Please select either ' + chalk.cyan.bold('(y/n) ');
                            }
                            say(' ');
                            rl.question(text, (answer) => {
                                if(answer.toLowerCase() == 'y' || answer.toLowerCase() == 'yes') {
                                    // Loop through and actually update the file names
                                    // -------------------------------------------------------------------------------
                                    helpers.maybeUpdateFiles(settings);

                                    say(chalk.green('\n\rSuccess!'));
                                    message += `${counter} file `;
                                    message += counter == 1 ? 'name was updated.' : 'names were updated.';
                                    message += counter > 0 ? ` All instances of ${chalk.red(needle)} have been replaced with ${chalk.green(replacement)} in the current working directory\n\r` : '';
                                    say(message);
                                    rl.close();
                                    process.exit();
                                }

                                if(answer.toLowerCase() == 'n' || answer.toLowerCase() == 'no') {
                                    say('No changes were made. ' + chalk.red('Exiting') + '.');
                                    say(' ');
                                    rl.close();
                                    process.exit();
                                }

                                getVarification(true);
                            });
                        }
                        getVarification(false);
                    }
                });
            });
        });
    }
}
